#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>


#define SIZE 64

unsigned long long convert_to_decimal(char* input);
void convert_to_binary(int* bits, unsigned long long input);
void init_perm(int* input, int* perm, int* output);
void output(int* array, int runs);
void expansion(int* expand, int* right);

int main(){

	int n;			// Represents the number of lines

	char key_str[16]; 								// input key in hex
	int  key_bits[64];								// input key in bits
	unsigned long long key_decimal;					//

	char plain_str[16];	// plaintext in hex
	int plain_bits[64];	// plaintext in bits

	unsigned long long plain_decimal;


	static int IP[64] = {
		58, 50, 42, 34, 26, 18, 10, 2,
		60, 52, 44, 36, 28, 20, 12, 4,
		62, 54, 46, 38, 30, 22, 14, 6,
		64, 56, 48, 40, 32, 24, 16, 8,
		57, 49, 41, 33, 25, 17, 9,  1,
		59, 51, 43, 35, 27, 19, 11, 3,
		61, 53, 45, 37, 29, 21, 13, 5,
		63, 55, 47, 39, 31, 23, 15, 7
	};


	scanf("%s", key_str);

	// Modular code
	key_decimal = convert_to_decimal(key_str);
	printf("Key (decimal): %llu\n", key_decimal);

	printf("Full Key: ");
	convert_to_binary(key_bits, key_decimal);
	printf("\n");

	// Scanning in the number of lines
	scanf("%d", &n);
	printf("The number of plaintext lines is: %d\n", n);


	// Shows us without th odd parity
	int length = 0;
	printf("Key without odd parity: ");
	for(int x = 0; x < SIZE; x++){
		if((x+1)%8 == 0) continue;

		printf("%d", key_bits[x]);
		length++;

		if((length) % 8 == 0) printf(" "); 	// HARDCODED

	}

	printf("\n");				// HARDCODED

	printf("The length of the key without odd parity is: %d\n", length);

	printf("\n\n");
	for(int i = 0; i < n; i++){

		scanf("%s", plain_str);
		plain_decimal = convert_to_decimal(plain_str);
		printf("Plaintext (decimal): %llu\n", plain_decimal);


		printf("Full Key: ");
		convert_to_binary(plain_bits, plain_decimal);
		printf("\n");


		int IP_output[64] = { 0 };
		int L_0[32] = { 0 };
		int R_0[32] = { 0 };
		int expan[48] = { 0 };



		init_perm(plain_bits, IP, IP_output);
		for(int j = 0; j < SIZE/2; j++){
			L_0[j] = IP_output[j];
			R_0[j] = IP_output[j+32];
		}

		printf("IP Output: ");
		output(IP_output, 64);

		printf("L_0: ");
		output(L_0, 32);

		printf("R_0: ");
		output(R_0, 32);

		for(int round=1; round <= 16; round++){
			// TODO expansion
			// TODO xor_oneE(round)
			// TODO substitution
			// TODO Permute!
			// TODO xor_two

			// TODO Dumping right into left
			// TODO Duming xor2 into right
		}

		// TODO Dumping values to swap
		//


	}
	return 0;
}


unsigned long long convert_to_decimal(char* input){
	return strtoull(input, NULL, 16);
}


void convert_to_binary(int* bits, unsigned long long input){
	int k, c, length = 0;
	for(c = SIZE - 1; c >= 0; c--){
		k = input >> c;
		bits[(SIZE - 1)- c] = (k & 1) ? 1 : 0;
		printf("%d", (k & 1) ? 1 : 0);
		length++;


		if(length % 8 == 0) printf(" ");	// HARDCODED
	}

	printf("\nThe length of the key with odd parity is: %d", length);
	// return bits;
}

void init_perm(int* input, int* perm, int* output){

	for(int i = 0; i < SIZE; i++){
		output[i] = input[perm[i] - 1];
	}
}

// TODO
void expansion(int* expand, int* right){

	// int exp[8][6], i, j, k;
	// for(i = 0; i < 8; i++){
	// 	for(j = 0; j < 6; )
	// }
}

void output(int* array, int runs){
	int length = 0;
	for(int x = 0; x < runs; x++){

		printf("%d", array[x]);
		length++;

		if((length) % 8 == 0) printf(" "); 	// HARDCODED

	}
	printf("\n");
}



/*** JAVA IMPLEMENTATION ***/

// // Switches the left half of the current block with the right half.
// public void switchHalves() {
// 	int[] temp = new int[32];
//
// 	// We're just doing a regular swap between 32 bits...
//
// 	for (int i=0; i<32; i++)
// 		temp[i] = block[i];
//
// 	for (int i=0; i<32; i++)
// 		block[i] = block[32+i];
//
// 	for (int i=32; i<64; i++)
// 		block[i] = temp[i-32];
// }


// // Runs round num of DES.
// public void round(int num) {
// 	int[] left = new int[32];
// 	int[] right = new int[32];
//
// 	// Copy in the left and right blocks into temporary arrays.
// 	for (int i=0; i<32; i++)
// 		left[i] = block[i];
// 	for (int i=0; i<32; i++)
// 		right[i] = block[32+i];
//
// 	// Expand the right block.
// 	int[] expanded = E(right);
//
// 	// This is the XOR we want.
// 	int[] xorans = XOR(expanded, roundkeys[num]);
//
// 	// Run the s-boxes on all the appropriate "blocks".
// 	int[] sboxout = Sboxes(xorans);
//
// 	// Permute the S-box output.
// 	int[] fout = Permute(sboxout, P);
//
// 	// Then do the necessary XOR.
// 	fout = XOR(fout, left);
//
// 	// Copy the blocks back into their proper place!
// 	for (int i=0; i<32; i++)
// 		block[i] = right[i];
// 	for (int i=0; i<32; i++)
// 		block[32+i] = fout[i];
// }
//
// // Returns the XOR of the bit streams a and b.
// public static int[] XOR(int[] a, int[] b) {
// 	int[] ans = new int[a.length];
// 	for (int i=0; i<a.length; i++)
// 		ans[i] = (a[i]+b[i])%2;
// 	return ans;
// }
//
//
//
// // Returns the output of putting the 48 bit input through the
// // 8 S-boxes.
// public int[] Sboxes(int[] input) {
// 	int[] ans = new int[32];
//
// 	for (int i=0; i<8; i++) {
//
// 		// Just hard-coded this part. There doesn't seem to be a more
// 		// elegant way...
// 		int row = 2*input[6*i] + input[6*i+5];
// 		int col = 8*input[6*i+1]+4*input[6*i+2]+2*input[6*i+3]+input[6*i+4];
//
// 		int temp = stables[i][row][col];
//
// 		// We have to store the base-10 answer in binary, so we strip off the
// 		// bits one-by-one, in the usual manner from the least to most significant.
// 		for (int j=3; j>=0; j--) {
// 			ans[4*i+j] = temp%2;
// 			temp /= 2;
// 		}
// 	}
// 	return ans;
//
// }
//
// // Set up the keys for each round.
// public void setKeys() {
// 	roundkeys = new int[16][48];
//
// 	// Set the original key with PC1.
// 	key = Permute(key, PC1);
//
// 	// Go through and set the round keys using the process by which they
// 	// are supposed to be computed.
// 	for (int i=0; i<16; i++) {
//
// 		// Supposed to left-shift both halves by the appropriate amount,
// 		// based on the round.
// 		leftShift(key, 0, 27, keyshifts[i]);
// 		leftShift(key, 28, 55, keyshifts[i]);
//
// 		// Now, just copy in the (i+1)th round key.
// 		for (int j=0; j<48; j++) {
// 			roundkeys[i][j] = key[PC2[j]-1];
// 		}
// 	}
// }
//
// // Converts the string version of the key in HEX to binary which is
// // stored in an integer array of size 64...thus, the check bits are
// // included here.
// public static int[] getKey(String thekey) {
// 	int[] ans = new int[64];
// 	thekey = thekey.toLowerCase();
//
// 	// Go through all 16 characters.
// 	for (int i=0; i<16; i++) {
// 		int val = (int)(thekey.charAt(i));
//
// 		// We need to assign value separately if it is a digit or a letter.
// 		if ('0' <= val && val <= '9')
// 			val = val - '0';
// 		else
// 			val = val - 'a' + 10;
//
// 		// Peel off the binary bits as before...
// 		for (int j=3; j>=0; j--) {
// 			ans[4*i+j]=val%2;
// 			val /= 2;
// 		}
// 	}
//
// 	return ans;
// }
